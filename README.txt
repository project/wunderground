DESCRIPTION
-----------

This module takes xml feeds from wunderground.com and displays weather information in blocks.

Features include:
* Multiblock (http://drupal.org/project/multiblock) module support
* Caching of feeds
* Integration with locations (http://drupal.org/project/location) module to find weather at user locations
* Lots of options!

Module comes with three kinds of blocks
* PWS (personal weather station) Block: Choose from thousands of user contributed weather stations around
  the world that serve detailed weather readings.
* User Local Forecast Block: If users have entered a location through the user locations module, this block will
  display a six day forecast for that location, as a list or as a jquery slider.
* Weather Lookup Block: Users can enter in a location and query the Wunderground feed for current weather.
  Uses autocomplete functionality to help users pick.

INSTALLATION
------------

* Download the module
* Enable the module
* If you are going to use the user local forecast block, download the location module and enable Location and
  User Location. However, if you just want this block to display weather at a default location, you do not need the
  Location module.
* Assign the "access wunderground content" permission to all user roles that you want to be able to see the
  Wunderground blocks.
* Go to Site Configuration -> Wunderground and set things like timezone, default location and default PWS ID.
* Place the blocks!

COMMON ISSUES
-------------

* If pages containing Wunderground blocks throw Javascript errors, make sure you are using a theme that includes
  block ids.

THEMING
-------

In this version, we made an effort to include all block content in theme functions for easy theming access.