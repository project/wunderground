<?php

/**
 * @file
 * Contains \Drupal\wunderground\Controller\WundergroundController.
 */

namespace Drupal\wunderground\Controller;

use Drupal\Core\Controller\ControllerBase;
use Guzzle\Http\Client;

/**
 * Returns responses for devel module routes.
 */
class WundergroundController extends ControllerBase {

  /**
   * Wunderground API key.
   * 
   * @var string
   */
  protected $key;

  /**
   * Max API calls allowed per minute.
   * 
   * @var int
   */
  protected $perMinAllowed;

  /**
   * Current API calls per minute.
   * 
   * @var string
   */
  protected $perMinCurrent;

  /**
   * Max Calls per day allowed.
   * 
   * @var int
   */
  protected $perDayAllowed;

  /**
   * Current calls per day.
   * 
   * @var string
   */
  protected $perDayCurrent;

  /**
   * Fahrenheit / Celcius declaration.
   * 
   * @var string
   */
  protected $method;

  /**
   * Single character detailing the Wunderground icon set to use.
   * 
   * @var string
   */
  protected $iconSet;

  /**
   * Custom icons possibility.
   * 
   * @var bool
   */
  protected $customIcons;

  /**
   * Custom icon directory.
   * 
   * @var string
   */
  protected $iconDir;

  /**
   * Extension for the icon format.
   * 
   * @var string
   */
  protected $iconFormat;

  /**
   * Constructor for the Wunderground Class.
   */
  public function __construct() {

    $config = $this->config('wunderground.settings')->get();
    $config['method'] == 0 ? $this->method = 'fahrenheit' : $this->method = 'celcius';

    $this->key = $config['api']['key'];
    $this->perMinAllowed = $config['api']['per_min']['allowed'];
    $this->perMinCurrent = $config['api']['per_min']['current'];
    $this->perDayAllowed = $config['api']['per_day']['allowed'];
    $this->perDayCurrent = $config['api']['per_day']['current'];

    $this->iconSet = $config['icons']['icon_set'];
    $this->customIcons = $config['icons']['use_custom_icons'];
    $this->iconDir = $config['icons']['icon_dir'];
    $this->iconFormat = $config['icons']['icon_format'];

    // Show the admin if we have exceeded our API calls.
    if ($this->key) {
      $this->wundergroundApiCtr($write = FALSE, $error = TRUE);
    }
  }

  /**
   * Locations Nearby.
   * 
   * This API call is to report locations nearby the IP address of the website.  It's
   * important to note that this function is called immediately after a saved key is added!
   * 
   * @param string $key 
   *   The API key used for connection.
   * 
   * @return array 
   *   A mix of PWS and Airports by the Zipcode.
   *
   * @todo Run the API counter on this static function.  Does this need to run separate instance?
   */
  public static function locationsNearby($key) {

    $client = new Client('http://api.wunderground.com/api');
    $request = $client->get($key . '/geolookup/q/autoip.json');
    $response = $request->send();
    $json = json_decode($response->getBody());

    // Execute conditional for nothing found...
    $airports = $json->location->nearby_weather_stations->airport->station;
    $pws = $json->location->nearby_weather_stations->pws->station;

    $stations = array();
    foreach ($airports as $res) {

      $city = preg_replace('/[^\da-z]/i', '_', $res->city);
      $loc = (string) $res->state . '/' . $city;
      $stations['airports'][$loc] = serialize($res);
    }
    foreach ($pws as $res) {

      $city = preg_replace('/[^\da-z]/i', '_', $res->city);
      $loc = (string) $res->state . '/' . $city;
      $stations['pws'][$loc] = serialize($res);
    }

    $config = config('wunderground.settings');
    $config->set('nearest.stations', $stations);
    $config->save();

    return;
  }

  /**
   * Report the default locations nearby.
   *
   * If their are locations nearby saved in our settings, dig them up for display.
   * 
   * @return array 
   *   Mixture of PWS / Airports.
   */
  public function nearbyDefaults() {

    $options = array();
    $stations = config('wunderground.settings')->get('nearest.stations');

    if (count($stations) == 0) {
      $options = array(0 => 'No stations found. API Error.');
    }
    elseif ($stations['pws'] == 0 && $stations['airports'] == 0) {
      $options = array(0 => 'No stations available');
    }
    else {
      foreach ($stations['airports'] as $k => $v) {
        $data = unserialize($v);

        $city = preg_replace('/[^\da-z]/i', '_', $data->city);
        $loc = (string) $data->state . '/' . $city;

        $options[$loc] = '[Airport] ' . $data->city . ', ' . $data->state;
      }
      foreach ($stations['pws'] as $k => $v) {
        $data = unserialize($v);

        $city = preg_replace('/[^\da-z]/i', '_', $data->city);
        $loc = (string) $data->state . '/' . $city;

        $options[$loc] = '[PWS] ' . $data->city . ', ' . $data->state;
      }
    }
    return $options;
  }

  /**
   * Search by zipcode.
   * 
   * @param int $zip 
   *   Required to search the API.
   * 
   * @return serial 
   *   Serialed array containing all of the station informaiton.
   */
  public function zipcodeSearch($zip) {

    if (!isset($this->key)) {
      return serialize(array(0 => 'No key available'));
    }

    $api_check = $this->wundergroundApiCtr($write = FALSE, $error = FALSE);
    if ($api_check['minctr'] == 0 && $api_check['dayctr'] == 0) {

      $client = new Client('http://api.wunderground.com/api');
      $request = $client->get($this->key . '/geolookup/q/' . $zip . '.json');
      $response = $request->send();
      $json = json_decode($response->getBody());

      $airports = $json->location->nearby_weather_stations->airport->station;
      $pws = $json->location->nearby_weather_stations->pws->station;

      $stations = array();
      foreach ($airports as $res) {
        $city = preg_replace('/[^\da-z]/i', '_', $res->city);
        $loc = (string) $res->state . '/' . $city;
        $stations['airports'][$loc] = serialize($res);
      }
      foreach ($pws as $res) {
        $city = preg_replace('/[^\da-z]/i', '_', $res->city);
        $loc = (string) $res->state . '/' . $city;
        $stations['pws'][$loc] = serialize($res);
      }

      // Always overwrite previous searches when this is attempted.
      $config = config('wunderground.settings');
      $config->set('lookups.zipcode', (int) $zip);
      $config->set('lookups.stations', $stations);
      $config->save();

      $this->wundergroundApiCtr($write = TRUE, $error = FALSE);
      return serialize($this->zipcodeSearchResults());
    }
    else {
      return serialize(array(0 => 'API Exceeded'));
    }
  }

  /**
   * Saved Sip Results.
   * 
   * @return array 
   *   Saved values in the settings file.
   */
  public function zipcodeSearchResults() {

    $options = array();
    $stations = config('wunderground.settings')->get('lookups.stations');

    if ($stations['pws'] == 0 && $stations['airports'] == 0) {
      $options = array(0 => 'No stations available');
    }
    else {
      foreach ($stations['airports'] as $k => $v) {
        $data = unserialize($v);
        $options[$k] = '[Airport] ' . $data->city . ', ' . $data->state;
      }
      foreach ($stations['pws'] as $k => $v) {
        $data = unserialize($v);
        $options[$k] = '[PWS] ' . $data->city . ', ' . $data->state;
      }
    }
    return $options;
  }

  /**
   * Wunderground Weather API call.
   *
   * This API call will be used for multiple methods from the block.
   * 
   * @param string $location 
   *   The location sent in is saved in our config (State/City Format)
   * 
   * @param string $type 
   *   The type of API we will use determined by the block entity settings.
   *
   * @return object 
   *   Object containing all of the weather information.
   */
  public function wundergroundGetWeather($location, $type) {

    if (!isset($this->key)) {
      return serialize(array(0 => 'No key available'));
    }

    $api_check = $this->wundergroundApiCtr($write = FALSE, $error = FALSE);
    if ($api_check['minctr'] == 0 && $api_check['dayctr'] == 0) {

      $client = new Client('http://api.wunderground.com/api');
      $request = $client->get($this->key . '/' . $type . '/q/' . $location . '.json');
      $response = $request->send();
      $json = json_decode($response->getBody());

      $this->wundergroundApiCtr($write = TRUE, $error = FALSE);
      return $json;
    }
    else {
      return 'Internal Error.';
    }
  }

  /**
   * Wunderground API Counter.  
   * 
   * This is extra credit.  The API counter will determine how many time we have
   * used the API per minute and per day.  This will restrict further API calls if
   * necessary.  We have the ability to simply check the status, check and write, check write
   * and show errors and simply show errors.
   * 
   * @return bool 
   *   Declare if the API can be used or not.
   */
  protected function wundergroundApiCtr($write = TRUE, $error = FALSE) {

    $config = config('wunderground.settings');
    $min = date('YmdHi');
    $day = date('Ymd');
    $min_ctr = 0;
    $day_ctr = 0;

    $permin = explode('|', $this->perMinCurrent);
    $perday = explode('|', $this->perDayCurrent);

    // Minute API verification.
    if ($permin[0] < $this->perMinAllowed && $min == $permin[1]) {
      // Increment.
      if ($write == TRUE) {
        $config->set('api.per_min.current', $permin[0] + 1 . '|' . $min);
      }
    }
    elseif ($permin[0] == $this->perMinAllowed && $min == $permin[1]) {
      // Exceeded.
      $min_ctr = 1;
      if ($error == TRUE) {

        $error_copy = 'Warning! API calls per minute exceeded: ' . $permin[0] . '/' . $this->perMinAllowed;
        drupal_set_message(t($error_copy), 'warning', FALSE);
      }
    }
    else {
      // Reset.
      if ($write == TRUE) {
        $config->set('api.per_min.current', 1 . '|' . $min);
      }
    }

    // Daily API verification.
    if ($perday[0] < $this->perDayAllowed && $day == $perday[1]) {
      // Increment.
      if ($write == TRUE) {
        $config->set('api.per_day.current', $perday[0] + 1 . '|' . $day);
      }
    }
    elseif ($perday[0] == $this->perDayAllowed && $day == $perday[1]) {
      // Exceeded.
      $day_ctr = 1;
      if ($error == TRUE) {
        $error_copy = 'Warning! Daily API calls exceeded: ' . $perday[0] . '/' . $this->perDayAllowed;
        drupal_set_message(t($error_copy), 'warning', FALSE);
      }
    }
    else {
      // Reset.
      if ($write == TRUE) {
        $config->set('api.per_day.current', 1 . '|' . $day);
      }
    }

    if ($write == TRUE) {
      $config->save();
    }

    return array('minctr' => $min_ctr, 'dayctr' => $day_ctr);
  }

  /**
   * Get Weather Location.
   * 
   * @param bool $var 
   *   The location to used is determined in the selection of the config.
   * 
   * @return string 
   *   String used to help parse the settings.
   */
  public function getWeatherLocation($var) {

    // Near you selected by admin.
    $var == 1 ? $output = 'nearest' : $output = 'lookups';
    return $output;
  }

  /**
   * Build Current Weather.
   *
   * @param object $weather 
   *   JSON object containing all of the weather for this method.
   *
   * @return array 
   *   Return an array containing data for the template to use.
   */
  public function buildCurrentWeather($weather) {

    $units = $this->getUnits();
    $data = $weather->current_observation;

    $output = array(
      'theme' => 'current_weather',
      'data' => array(
        'display_location_full' => $data->display_location->full,
        'display_location_city' => $data->display_location->city,
        'observation_location_full' => $data->observation_location->full,
        'observation_location_city' => $data->observation_location->city,
        'observation_time' => $data->observation_epoch,
        'icon' => $this->getWeatherIcon($data->icon, $data->observation_epoch),
        'weather' => $data->weather,
        'temp' => ($units['symbol'] == 'f' ? $data->temp_f : $data->temp_c) . ' &#176;' . $units['symbol'],
        'wind_string' => $data->wind_string,
        'wind_direction' => $data->wind_dir,
        'wind_speed' => ($units['speed'] == 'mph' ? $data->wind_mph : $data->wind_kph),
        'wind_chill' => ($units['symbol'] == 'f' ? $data->windchill_f : $data->windchill_c) . ' &#176;' . $units['symbol'],
        'dewpoint' => ($units['symbol'] == 'f' ? $data->dewpoint_f : $data->dewpoint_c) . ' &#176;' . $units['symbol'],
        'feelslike' => ($units['symbol'] == 'f' ? $data->feelslike_f : $data->feelslike_c) . ' &#176;' . $units['symbol'],
        'visibility' => ($units['distance'] == 'mi' ? $data->visibility_mi : $data->visibility_km) . ' ' . $units['distance'],
        'humidity' => $data->relative_humidity,
      ),
    );

    return $output;
  }

  /**
   * Build Hourly Weather.
   *
   * @param object $weather 
   *   JSON object containing all of the weather for this method.
   *
   * @return array 
   *   Return an array containing data for the template to use.
   */
  public function buildHourlyWeather($weather) {

    $units = $this->getUnits();
    $hourly_data = array();
    foreach ($weather->hourly_forecast as $k => $hourly) {

      $hourly_data[] = array(
        'month' => $hourly->FCTTIME->month_name,
        'time' => $hourly->FCTTIME->civil,
        'timestamp' => $hourly->FCTTIME->epoch,
        'weekday_name' => $hourly->FCTTIME->weekday_name,
        'ampm' => $hourly->FCTTIME->ampm,
        'date_custom' => date('M d g:ia', $hourly->FCTTIME->epoch),
        'temp' => $hourly->temp->$units['method'] . ' &#176;' . $units['symbol'],
        'dewpoint' => $hourly->dewpoint->$units['method'],
        'condition' => $hourly->condition,
        'icon' => $icon = $this->getWeatherIcon($hourly->icon, $hourly->FCTTIME->epoch),
        'wind_speed' => $hourly->wspd->$units['method'],
        'wind_direction' => $hourly->wdir->dir,
        'desc' => $hourly->wx,
        'humidity' => $hourly->humidity,
        'windchill' => $hourly->windchill->$units['method'],
        'heatindex' => $hourly->heatindex->$units['method'],
        'feelslike' => $hourly->feelslike->$units['method'],
      );

      if ($k == 5) {
        break;
      }
    }

    $output = array(
      'theme' => 'hourly',
      'data' => $hourly_data,
    );

    return $output;
  }

  /**
   * Build Forecast Weather.
   *
   * @param object $weather 
   *   JSON object containing all of the weather for this method.
   *
   * @return array 
   *   Return an array containing data for the template to use.
   */
  public function buildForecastWeather($weather) {

    $text = $weather->forecast->txt_forecast->forecastday;
    $simple = $weather->forecast->simpleforecast->forecastday;
    $units = $this->getUnits();

    $method = $this->method;

    $weekly_data = array();
    foreach ($text as $k => $v) {

      $units['symbol'] == 'f' ? $desc = 'fcttext' : $desc = 'fcttext_metric';
      $weekly_data[] = array(
        'icon' => $this->getWeatherIcon($v->icon, 0),
        'title' => $v->title,
        'desc' => $v->$desc,
      );
    }
    $output = array(
      'theme' => 'forecast',
      'data' => $weekly_data,
    );

    return $output;
  }

  /**
   * Build Satellite Weather.
   *
   * @param object $weather 
   *   JSON object containing all of the weather for this method.
   *
   * @return array 
   *   Return an array containing data for the template to use.
   */
  public function buildSatelliteWeather($weather) {

    $satellite_img = array();
    foreach ($weather->satellite as $v) {
      $satellite_img[] = htmlentities($v);
    }

    $output = array(
      'theme' => 'satellite',
      'data' => $satellite_img,
    );

    return $output;
  }

  /**
   * Get Weather Icon.
   *
   * A function reused which will determine if an icon set from The Wunderground will be 
   * used or if a custom icon directory will be used.  If the custom icon directory is used
   * and the icon cannot be found, we will default the the Wunderground icon.
   * 
   * @param string $icon 
   *   The icon name being passed in.
   *
   * @param int $time 
   *   The time setting for the icon.  This is used to determine day or night (pretty slick).
   *
   * @return string
   *   The icon with directory or the wundergound icon URL.
   */
  protected function getWeatherIcon($icon, $time) {

    // Determine day or night.
    $icon_prefix = 'nt_';
    $now = date('G', $time);
    if ($now > 7 && $now < 20 || $time == 0) {
      $icon_prefix = NULL;
    }

    $custom_icon = $this->iconDir . $icon_prefix . $icon . '.' . $this->iconFormat;
    if ($this->customIcons == 1 && file_exists($custom_icon)) {
      $icon = $custom_icon;
    }
    else {
      $icon = 'http://icons-ak.wxug.com/i/c/' . $this->iconSet . '/' . $icon_prefix . $icon . '.gif';
    }

    return $icon;
  }

  /**
   * Units.
   *
   * A reused function to determine the units and measurements that we use to
   * help parse the API.
   * 
   * @return array
   *   The array of unit information.
   */
  protected function getUnits() {

    $units = array();
    if ($this->method == 'fahrenheit') {
      $unit['method'] = 'english';
      $unit['symbol'] = 'f';
      $unit['distance'] = 'mi';
      $unit['length'] = 'in';
      $unit['speed'] = 'mph';
    }
    elseif ($this->method == 'celcius') {
      $unit['method'] = 'metric';
      $unit['symbol'] = 'c';
      $unit['distance'] = 'km';
      $unit['length'] = 'mm';
      $unit['speed'] = 'kph';
    }

    return $unit;
  }
}
