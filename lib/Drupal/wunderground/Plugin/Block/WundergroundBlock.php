<?php

/**
 * @file
 * Contains \Drupal\wunderground\Plugin\Block\WundergroundBlock.
 */

namespace Drupal\wunderground\Plugin\Block;

use Drupal\block\BlockBase;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Session\AccountInterface;
use \Drupal\wunderground\Controller\WundergroundController;

/**
 * Provides a simple block.
 *
 * @Block(
 *   id = "wunderground_block",
 *   admin_label = @Translation("Wunderground Weather")
 * )
 */
class WundergroundBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array(
      'cache' => DRUPAL_CACHE_GLOBAL,
      'block_mode' => "all pages",
    );
  }

  /**
   * Implements \Drupal\block\BlockBase::blockBuild().
   */
  public function build() {

    $obj = new WundergroundController();
    $config_settings = \Drupal::Config('wunderground.settings')->get();

    if (!isset($config_settings['api']['key'])) {
      drupal_set_message(t('No Wunderground API key found!'), 'error', FALSE);
      return;
    }

    $file = 'block.block.' . $this->configuration['formid'];

    $block_settings = config($file);
    $time = $block_settings->get('settings.time');
    $data = $block_settings->get('settings.dataselection');

    $weather = unserialize($block_settings->get('settings.weather'));
    $loc = $obj->getWeatherLocation($config_settings['selection']);

    if (!stristr($config_settings[$loc]['default'], '/')) {
      drupal_set_message(t('A default location has not been set in the configuration settings.'), 'error', FALSE);
      return;
    }
    else {
      $location = $config_settings[$loc]['default'];
    }

    // Run API call to get the updated weather when cache expires or if an error is returned.
    if (time() >= ($time + $config_settings['cache'])) {

      $weather = $obj->wundergroundGetWeather($location, $data);
      $block_settings->set('settings.time', time());
      $block_settings->set('settings.weather', serialize($weather));
      $block_settings->save();
    }

    if ($data == 'conditions') {
      $output = $obj->buildCurrentWeather($weather);
    }
    elseif ($data == 'hourly') {
      $output = $obj->buildHourlyWeather($weather);
    }
    elseif ($data == 'forecast') {
      $output = $obj->buildForecastWeather($weather);
    }
    elseif ($data == 'satellite') {
      $output = $obj->buildSatelliteWeather($weather);
    }

    return array(
      '#theme' => $output['theme'],
      '#weather' => $output['data'],
      '#attached' => array(
        'css' => array(drupal_get_path('module', 'wunderground') . '/css/wunderground.css'),
      ),
    );
  }

  /**
   * Overrides \Drupal\block\BlockBase::blockForm().
   */
  public function blockForm($form, &$form_state) {

    $form['#attached']['css'] = array(
      drupal_get_path('module', 'wunderground') . '/css/wunderground-admin.css',
    );

    $form['wunderground_settings'] = array(
      '#type' => 'details',
      '#title' => $this->t('Current Weather Display Settings'),
      '#description' => 'Select which type of data you would like the Wunderground API to extract.  Note:  You can override the theme by placing the template in your themes directory!',
      '#collapsed' => FALSE,
      '#group' => 'wunderground',
      '#weight' => 1,
    );

    $form['wunderground_settings']['dataselection'] = array(
      '#type' => 'select',
      '#title' => 'Options',
      '#options' => array(
        'conditions' => 'Current',
        'hourly' => 'Hourly',
        'forecast' => 'Forecast',
        'satellite' => 'Satellite',
      ),
      '#default_value' => isset($this->configuration['dataselection']) ? array($this->configuration['dataselection']) : 'conditions',
    );

    return $form;
  }

  /**
   * Overrides \Drupal\block\BlockBase::blockSubmit().
   */
  public function blockSubmit($form, &$form_state) {

    // Formid is the only way I can draw the yml file, so keep this setting!
    $formid = $form['id']['#default_value'];
    $this->configuration['formid'] = $formid;
    $this->configuration['dataselection'] = $form_state['values']['wunderground_settings']['dataselection'];
  }


  /**
   * Implements \Drupal\block\BlockBase::access().
   */
  public function access(AccountInterface $account) {
    return $account->hasPermission('access content');
  }
}
