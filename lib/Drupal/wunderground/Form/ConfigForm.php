<?php

/**
 * @file
 * Contains \Drupal\devel\Form\ConfigEditor.
 */

namespace Drupal\wunderground\Form;

use Drupal\Core\Form\FormBase;
use Drupal\wunderground\Controller\WundergroundController;

/**
 * Edit config variable form.
 */
class ConfigForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'wunderground_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {

    $obj = new WundergroundController();
    $config = $this->config('wunderground.settings')->get();

    $apikey = $config['api']['key'];
    $zipcode = $config['lookups']['zipcode'];
    $permin = $config['api']['per_min']['allowed'];
    $perday = $config['api']['per_day']['allowed'];

    $permin_current = explode('|', $config['api']['per_min']['current']);
    $perday_current = explode('|', $config['api']['per_day']['current']);

    $method = $config['method'];
    $cache = $config['cache'];
    $nearest_default = $config['nearest']['default'];
    $lookups_default = $config['lookups']['default'];
    $selection = $config['selection'];

    $form['html'] = array(
      '#type' => 'markup',
      '#markup' => '<p>Below are the configuration settings for the Drupal 8 ' . l(t('Wunderground'), 'http://www.wunderground.com/') . ' module.</p>',
    );

    // Api Configuration.
    $form['wunderground_api'] = array(
      '#type' => 'details',
      '#title' => $this->t('Wunderground Key'),
      '#collapsed' => $apikey ? TRUE : FALSE,
      '#description' => $this->t('You are required to have an ' . l(t('API Key'), 'http://www.wunderground.com/weather/api/d/questionnaire.html?plan=a&level=0&history=0') . ' in order to use this module.'),
      '#group' => 'api',
    );
    $form['wunderground_api']['key'] = array(
      '#type' => 'textfield',
      '#title' => 'API Key',
      '#default_value' => $apikey,
    );

    // Settings configuration.
    $form['wunderground_settings'] = array(
      '#type' => 'details',
      '#title' => $this->t('Module Settings'),
      '#collapsed' => $apikey ? FALSE : TRUE,
      '#description' => $this->t('Configure the default settings for how you want the module to function.  <br />Note: The default allotted API calls are based on the free plan from Wunderground (Developer Plan).'),
      '#group' => 'settings',
    );
    $form['wunderground_settings']['method'] = array(
      '#type' => 'radios',
      '#title' => 'Method',
      '#options' => array(
        0 => 'Fahrenheit',
        1 => 'Celsius',
      ),
      '#default_value' => $method ? $method : 0,
    );
    $form['wunderground_settings']['callsperday'] = array(
      '#type' => 'select',
      '#title' => 'MAX API Calls Per Day',
      '#description' => 'Currently used ' . $perday_current[0] . '/' . $perday,
      '#default_value' => $perday > 500 ? $perday : 500,
      '#options' => array(
        500 => '500',
        5000 => '5,000',
        100000 => '100,000',
        1000000 => '1,000,000',
      ),
    );
    $form['wunderground_settings']['callspermin'] = array(
      '#type' => 'select',
      '#title' => 'MAX API Calls Per Minute',
      '#description' => 'Currently used ' . $permin_current[0] . '/' . $permin,
      '#default_value' => $permin > 10 ? $permin : 10,
      '#options' => array(
        10 => '10',
        100 => '100',
        1000 => '1000',
        10000 => '10,000',
      ),
    );
    $form['wunderground_settings']['cache'] = array(
      '#type' => 'select',
      '#title' => 'Refresh Rate',
      '#description' => 'How often would you like to refresh the weather?',
      '#default_value' => $cache ? $cache : 3600,
      '#options' => array(
        60 * 5 => '5 Minutes',
        60 * 30 => '30 Minutes',
        60 * 60 => '1 Hour',
        60 * 60 * 6 => '6 Hours',
        60 * 60 * 12 => '12 Hours',
        60 * 60 * 24 => '1 Day',
      ),
    );

    // Location information.
    $form['wunderground_location'] = array(
      '#type' => 'details',
      '#title' => $this->t('Location Configuration'),
      '#collapsed' => $apikey ? FALSE : TRUE,
      '#description' => $this->t('Select from a predefined location determined by your IP address, or you can search by zipcode and use that.'),
      '#group' => 'location',
    );
    $form['wunderground_location']['nearby'] = array(
      '#type' => 'select',
      '#title' => 'Weather Stations Nearby',
      '#options' => $obj->nearbyDefaults(),
      '#default_value' => $nearest_default ? $nearest_default : '',
    );
    $form['wunderground_location']['zipcode'] = array(
      '#type' => 'textfield',
      '#title' => 'Zipcode',
      '#default_value' => $zipcode,
      '#description' => "Enter in a zipcode and we will search for weather stations near it.",
      '#attributes' => array(
        'style' => 'width:150px',
      ),
    );
    $form['wunderground_location']['usersearch'] = array(
      '#type' => 'select',
      '#title' => 'Stations near Zipcode',
      '#options' => @$form_state['search_by_zip'] ? unserialize($form_state['search_by_zip']) : $obj->zipcodeSearchResults(),
      '#default_value' => $lookups_default ? $lookups_default : '',
      '#prefix' => '<div id="zip-replace">',
      '#suffix' => '</div>',
    );
    $form['wunderground_location']['add'] = array(
      '#type' => 'submit',
      '#value' => t('Check Zipcode'),
      '#submit' => array(array($this, 'SearchByZipSubmit')),
      '#ajax' => array(
        'callback' => array($this, 'SearchByZipCallback'),
        'wrapper' => 'zip-replace',
        'effect' => 'fade',
      ),
      '#attributes' => array(
        'style' => 'margin:0',
      ),
    );
    $form['wunderground_location']['selection'] = array(
      '#type' => 'radios',
      '#title' => 'Station Select',
      '#description' => 'The selection made below will determine the weather locaiton shown on your website.',
      '#options' => array(
        1 => 'Nearest You',
        2 => 'BY Zipcode',
      ),
      '#default_value' => $selection ? $selection : 1,
    );

    // Location information.
    $form['wunderground_icons'] = array(
      '#type' => 'details',
      '#title' => 'Weather Icons',
      '#description' => 'If you would like to add custom icons or simply use an alternate icon set for your website, please use the options below.',
      '#collapsed' => TRUE,
      '#group' => 'icons',
    );

    $form['wunderground_icons']['icon_set'] = array(
      '#type' => 'select',
      '#title' => 'Icon Set',
      '#description' => 'Choose from 11 different icon sets found at the ' . l(t('Wunderground'), 'http://www.wunderground.com/weather/api/d/docs?d=resources/icon-sets'),
      '#options' => array(
        'a' => 'Set 1',
        'b' => 'Set 2',
        'c' => 'Set 3',
        'd' => 'Set 4',
        'e' => 'Set 5',
        'f' => 'Set 6',
        'g' => 'Set 7',
        'h' => 'Set 8',
        'i' => 'Set 9',
        'j' => 'Set 10',
        'k' => 'Set 11',
      ),
      '#default_value' => $config['icons']['icon_set'] ? array($config['icons']['icon_set']) : array('k'),
    );

    $form['wunderground_icons']['use_custom_icons'] = array(
      '#type' => 'checkbox',
      '#title' => 'Use custom icons for your weather output',
      '#default_value' => $config['icons']['use_custom_icons'] ? $config['icons']['use_custom_icons'] : 0,
    );
    $form['wunderground_icons']['custom']['icon_settings'] = array(
      '#type' => 'container',
      '#states' => array(
        // Hide the additional settings when this email is disabled.
        'invisible' => array(
          'input[name="use_custom_icons"]' => array('checked' => FALSE),
        ),
      ),
    );

    $form['wunderground_icons']['custom']['icon_settings']['markup'] = array(
      '#type' => 'markup',
      '#markup' => '
      <p><strong>The filename for your icons will use one of the following:</strong></p>
      <fieldset><legend>Icon Name options</legend>
      <ul id="day-icons">
      <li><strong>Day: </strong></li>
      <li>chanceflurries</li>
      <li>chancerain</li>
      <li>chancesleet</li>
      <li>chancesnow</li>
      <li>chancetstorms</li>
      <li>clear</li>
      <li>cloudy</li>
      <li>flurries</li>
      <li>fog</li>
      <li>hazy</li>
      <li>mostlycloudy</li>
      <li>mostlysunny</li>
      <li>partlycloudy</li>
      <li>partlysunny</li>
      <li>sleet</li>
      <li>rain</li>
      <li>snow</li>
      <li>sunny</li>
      <li>tstorms</li>
      <li>cloudy</li>
      <li>partlycloudy</li>
      </ul>
      <ul id="night-icons">
      <li><strong>Night: </strong></li>
      <li>nt_chanceflurries</li>
      <li>nt_chancerain</li>
      <li>nt_chancesleet</li>
      <li>nt_chancesnow</li>
      <li>nt_chancetstorms</li>
      <li>nt_clear</li>
      <li>nt_cloudy</li>
      <li>nt_flurries</li>
      <li>nt_fog</li>
      <li>nt_hazy</li>
      <li>nt_mostlycloudy</li>
      <li>nt_mostlysunny</li>
      <li>nt_partlycloudy</li>
      <li>nt_partlysunny</li>
      <li>nt_sleet</li>
      <li>nt_rain</li>
      <li>nt_snow</li>
      <li>nt_sunny</li>
      <li>nt_tstorms</li>
      <li>nt_cloudy</li>
      <li>nt_partlycloudy</li>
      </ul>
      </fieldset>
      ',
    );

    // checkbox... if selected, override the Wunderground icons.
    $form['wunderground_icons']['custom']['icon_settings']['icon_dir'] = array(
      '#type' => 'textfield',
      '#description' => 'eg. themes/zen/icons/  <br /> Note: Your filename will end up referring to something like: themes/zen/icons/nt_chancetstorms.png',
      '#title' => 'Icon Directory',
      '#default_value' => $config['icons']['icon_dir'],
    );
    $form['wunderground_icons']['custom']['icon_settings']['icon_format'] = array(
      '#type' => 'select',
      '#title' => 'Icon Format',
      '#options' => array(
        'png' => 'png',
        'jpg' => 'jpg',
        'jpeg' => 'jpeg',
        'gif' => 'gif',
      ),
      '#default_value' => array($config['icons']['icon_format']),
    );
    $form['wunderground_icons']['custom']['icon_settings']['markuplower'] = array(
      '#type' => 'markup',
      '#markup' => '<p>Note: If the file is not found, we will use use the icon from the set provided by the Wunderground.</p>',
    );

    $form['#attached']['css'] = array(
      drupal_get_path('module', 'wunderground') . '/css/wunderground-admin.css',
    );

    // Submit and form reset actions.
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
    $form['actions']['reset'] = array('#type' => 'submit', '#value' => t('Reset'));

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function SearchByZipSubmit(array &$form, array &$form_state) {

    $obj = new WundergroundController();
    $response = $obj->zipcodeSearch($form_state['values']['zipcode']);

    $form_state['search_by_zip'] = $response;
    $form_state['rebuild'] = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function SearchByZipCallback(array &$form, array &$form_state) {
    return $form['wunderground_location']['usersearch'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {

    $key = $form_state['values']['key'];

    // If the key changes or if it's the first instance of loading the key, reach out and
    // try to dig up locations for them to choose from nearby.
    if ($key != $this->config('wunderground.settings')->get('api.key')) {
      WundergroundController::locationsNearby($key);
    }

    $this->configFactory->get('wunderground.settings')
    ->set('api.key', $key)
    ->set('api.per_min.allowed', (int) $form_state['values']['callspermin'])
    ->set('api.per_day.allowed', (int) $form_state['values']['callsperday'])
    ->set('method', $method = (int) $form_state['values']['method'])
    ->set('cache', (int) $form_state['values']['cache'])
    ->set('nearest.default', $form_state['values']['nearby'])
    ->set('lookups.default', $form_state['values']['usersearch'])
    ->set('selection', (int) $form_state['values']['selection'])

    ->set('icons.icon_set', (string) $form_state['values']['icon_set'])
    ->set('icons.use_custom_icons', (int) $form_state['values']['use_custom_icons'])
    ->set('icons.icon_dir', (string) $form_state['values']['icon_dir'])
    ->set('icons.icon_format', (string) $form_state['values']['icon_format'])
    ->save();

    drupal_set_message(t('Configuration settings saved.'));
  }
}
